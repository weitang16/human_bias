\documentclass[11pt]{article}

\title{Extensions on Biased Bandits}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{fullpage}
\usepackage{epsfig}
\usepackage{amsfonts}
\usepackage{MnSymbol}
\usepackage{graphicx}
\usepackage[numbers]{natbib}
\usepackage{xcolor}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{microtype}
\usepackage{indentfirst}
\usepackage{xspace}
\usepackage{enumitem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conj}[theorem]{Conjecture}
\newtheorem{assm}[theorem]{Assumption}
\newtheorem{comment}[theorem]{Comment}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newenvironment{sproof}{%
  \renewcommand{\proofname}{Proof Sketch}\proof}{\endproof}



%mathbb
\newcommand{\N}{\mathbb{N}}
\newcommand{\R}{\mathbb{R}}
%mathcal
\newcommand{\cA}{\mathcal{A}}
\newcommand{\cD}{\mathcal{D}}

% Expectation, Probability, Variance
\newcommand{\Esymb}{\mathbb{E}}
\newcommand{\Psymb}{\mathbb{P}}
\newcommand{\Vsymb}{\mathbb{V}}
\DeclareMathOperator*{\E}{\Esymb}
\DeclareMathOperator*{\Var}{\Vsymb}
\DeclareMathOperator*{\ProbOp}{\Psymb r}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\renewcommand{\Pr}{\ProbOp}
\newcommand{\prob}[1]{\Pr\big\{ #1 \big\}}
\newcommand{\Prob}[1]{\Pr\left\{ #1 \right\}}
\newcommand{\varprob}[1]{\Pr\big( #1 \big)}
\newcommand{\varProb}[1]{\Pr\left( #1 \right)}
\newcommand{\ex}[1]{\E\big[#1\big]}
\newcommand{\Ex}[1]{\E\left[#1\right]}
\newcommand{\varex}[1]{\E\paren{#1}}
\newcommand{\varEx}[1]{\E\Paren{#1}}
\newcommand{\widebar}[1]{\overline{#1}}

\newcommand{\pp}{\textsc{principal}\xspace}
\newcommand{\at}{\textsc{agent}\xspace}
\newcommand{\At}{\textsc{Agent}\xspace}


\renewcommand{\pp}{{principal}\xspace}
\renewcommand{\at}{{agent}\xspace}
\renewcommand{\At}{{Agent}\xspace}
\renewcommand{\hat}{\widehat}


\newcommand{\feedback}{\texttt{Feedback}}
\newcommand{\selection}{\texttt{Selection}}




\newif\ifcomment
\commentfalse

% \ifcomment
% \newcommand{\wt}[1]{\textcolor{blue}{[WT: #1]}}
% \else
% \newcommand{\wt}[1]{}
% \fi

\ifodd 1
\newcommand{\wt}[1]{{\color{blue}(WT: #1)}}
\else
\newcommand{\wt}[1]{}
\fi
\newcommand{\ignore}[1]{}

\newif\ifsupp
\supptrue

\setlength\marginparwidth{60pt}
\setlength\marginparsep{5pt}

\begin{document}
\maketitle

%----------------------------------------------------------------
\section{Introduction}
Empricial studies have showed that people are more likely to click the ads which are placed in a conspicuous slot among the website layout \cite{richardson2007predicting,craswell2008experimental,joachims2017accurately}, and more positive ratings of an item will also induce more positive ratings, i.e., many rating distributions are J-shaped with mostly 5-star ratings, some 1-star ratings, and hardly any ratings in between \cite{hu2006can}.


\section{Formulation}
We propose a universal framwork to incoperate human bias, especially the \emph{position bias} and \emph{social bias}, into an online learning problem, i.e., the item selection and the feedback provided by the agent may be biased by its history information and its postition. 

Formally, let $K$ be the number of arms.
Each arm $k \in [K] = \{1,...,K\}$ is associated with an unknown quality $\theta_{k} \in [0,1]$. 
Let $I^* = \argmax_k \theta_k$ and $\theta^* = \theta_{I^*}$ be the index of the best arm and the associated highest expected quality.
And assume there're $L$ slots in a website layout, these slots are associated with examination parameters $(\kappa_l)_{1\leq l \leq L}$ (known to the principal), where $\kappa_l \in (0,1)$ is the probability that the agent effectively observes the item in position $l$ \cite{lagree2016multiple}. 
Without loss of generality, assume that $\theta_1 > \theta_2 > ... > \theta_K$ and $\kappa_1 > \kappa_2> ...> \kappa_L$.

At each round $t$, the principal selects a list of $L$ arms associated with their history information - referred to as an \textit{action} - putting each arm into the corresponding slot.
The set of actions is denoted by $\mathcal{A}$ and thus contains $K!/(K-L)!$ ordered lists, the action selected at time $t$ will be denoted $A_t = \{A_t^1,..., A_t^L\}$.
Due to the existence of the position bias and social influence, the agent may not effectively observe all the items.
Intuitively, the agents are incline to select the arm which has higher observation probability and more positive social information in the sense to maximize their instant experience $X_t \in \{0,1 \}$.
Thus, let $I_t \in A_t$ be the selected arm from the agent. 
Once the agent finishing the arm selection, they will expeience this arm and then are required to provide a binary feedback $Z_t \in \{0,1\}$ on their experience (i.e., whether their experience is positive or negative).
We characterize two natural cases based on whether the agents' feedback are consistent with their private experience:
\begin{itemize}[label={}]
	\item \textbf{Truthful feedback.} the feedback is truthful with respect to their private experience, i.e.,
	\[
		X_t = Z_t  \sim \texttt{Bernoulli}[\theta_{I_t}].
	\]
	\item \textbf{Biased feedback.} the feedback is biased by the social information, i.e., in general, $\Esymb[X_t] \neq \Esymb[Z_t]$.
	Particularly, we use $\feedback(\theta, h)$ to model the probability of obtaining a positive feedback from users given that the arm quality is $\theta$ and the history information of the arm is summarized in $h = \{\rho, n\}$ where $\rho$ is its average positive feedback and $n$ denotes the total number of feedback , i.e., 
	\[
		X_t \sim \texttt{Bernoulli}[\theta_{I_t}], \quad  \feedback(\theta,h) = F(\theta, h), \quad Z_t \sim \texttt{Bernoulli}[F(\theta_{I_t},h_{I_t})]
	\]
\end{itemize}
Let $H_t = \{h_{A_t^1}, ..., h_{A_t^L}\}$ denote the associated history information for the selected action $A_t$ by \pp. 
We use $\selection(A_t, H_t)$ to model the probability measure for agent selecting arms, i.e.,
\[
	\Delta(A_t) = \selection(A_t, H_t) = S(A_t, H_t)
\]
Particularly, let $p_{A_t^l}$ denote the probabiltiy for agent selecting the arm $A_t^l$.





% the principal selects an subset arms $I_t \in \{1,...,K\}$ for the arriving user. 
% The user then gets a binary reward $Z_t$ (positive or negative experience) with 
% \[
%     Z_t \sim \texttt{Bernoulli}[\theta_{I_t}].
% \]

% The reward is not observable to the principal.
% However, after receiving the reward, each user provides a binary feedback $X_t \in \{0,1\}$ about this arm. 
% The goal of the principal is to maximize the total rewards users receive while observing only the (potentially biased) feedback.
% %We say feedback $X_t$ is \emph{biased} if $X_t \neq Z_t$. 

% \textbf{Dynamics:} 
% Denote $O(t) \subseteq A(t)$ as the agnet's observed items in round $t$. 
% Since the agent can also observe the information pertaining to the earlier agents' experience and he is bayesian rational and mypotic, then he will select the "perceived best" item based on his updated bayesian posterior.
% Let $I_t$ be the selected item by the agent in time round $t$, then:
% \begin{align}
% I_t = \argmax_{j \in O(t)} \frac{s_{t-1}^j + s_0^j}{s_{t-1}^j + s_0^j + f_{t-1}^j + f_0^j}
% \end{align}
% Also let this selected item's position is $S_{t} \in \{1,...,L\}$, i.e., $A(t)[S_{t}] = I_t$.


% \paragraph{User feedback models.}
% Users' feedback depend on both the realized rewards and the feedback history of the arms.
% We assume users have access to the historical feedback information for the arms selected in their rounds. 
% However, they have no access to the information of the other arms. 

% The feedback history of arm $k$ up to time $t$ can be summarized by $n_{k,t}$ and $\rho_{k,t}$, 
% which represent the number of feedback and the ratio of positive feedback for arm $k$ up to round $t$. 
% We assume $n_{k,0} = \rho_{k,0} = 0$ to simplify the presentation, however, our results easily extend to settings with non-zero $n_{k,0}$ and $\rho_{k,0}$,
% which can be used to represent the users' \emph{prior} of the arm quality.
% Note that if users provide unbiased feedback, we should have $X_t = Z_t$ for all $t$.
% However, in practice, user feedback might be biased by other users' feedback (i.e., the feedback history).

% In this paper, we use a feedback function to model the probability of obtaining a positive feedback from a user in the population.
% Note that a feedback function can be interpreted as describing the characteristic of the \emph{user population} the platform is interacting with, instead of describing specific users.
% In particular, we use $\feedback(\theta,\rho,n)$ to model the probability of obtaining a positive feedback from users given that the arm quality is $\theta$ and the history information of the arm is summarized by its average feedback $\rho$ and the number of feedback $n$.


% Naturally, when $\feedback(\theta,\rho,n) = \theta$, user feedback represents unbiased samples of the arm quality.

% In this paper, we explore two natural feedback models.

% In this feedback model, user feedback is biased by the average feedback of the arm. In particular, the feedback function has the form
% \[
%     \feedback(\theta,\rho,n) = F(\theta,\rho)
% \]
% %In Section~\ref{sec:timeinvariant},
% In the discussion later,
% we study the stochastic process of user feedback specified by $F$ and discuss the impacts on the design of bandit algorithms.



%----------------------------------------------------------------
\newpage
\section{Earlier Formulation}

\subsection{Model}
We discuss how to take advantage of position bias to help the principal (platform) to solve the \textbf{agent}'s (consumer's) selection bias problem among multiple available items (goods).

\textbf{Agent Model:} Let there're $K$ items with the unknown quality: $\{\theta_k\}_{1\leq k \leq K}$ and $\theta_k \in (0,1)$. 
Initially, $\theta_k$ is known to the principal and the agents only to the extent of a common prior belief, which is expressed in the model through a Beta random vaiable with the shape parameters $\{s_0^k, f_0^k\}, s_0^k, f_0^k \in \mathbb{Z}_+$. 
The agents can experience an item and then get a bianry experience. 
We assume once the agent completes the experience of selected item, he then truthfully reports his binary experience $X_t \in \{0,1\}$ to the principal. 
The principal will update the item's history information according to $X_t$.\\
\indent Let $\mathcal{H}_t = \{s_{t-1}^k, f_{t-1}^k\}_{1\leq k \leq K}$ be the available information to the agents. 
The agents are assumed to be \textbf{bayesian rational} and \textbf{mypotic} \cite{mansour2015bayesian,papanastasiou2017crowdsourcing,kremer2014implementing,che2015optimal}, i.e., they choose the item by maximizing his "perceived" expected experience according to his updated bayesian posterior.

\textbf{Position Bias Model:} Assume there're $L$ slots in a website layout, these slots are associated with examination parameters $(\kappa_l)_{1\leq l \leq L}$ (known to the principal), where $\kappa_l \in (0,1)$ is the probability that the agent effectively observes the item in position $l$ \cite{lagree2016multiple}. 

\textbf{Dynamics:} At the begining of each time round $t \in T, T= \{1,2,...\}$, the principal selects a list of $L$ items - referred to as an \textit{action} - chosen among the $K$ items which are indexed by $k \in \{1,..., K\}$. 
The set of actions is denoted by $\mathcal{A}$ and thus contains $K!/(K-L)!$ ordered lists; the action selected at time $t$ will be denoted $A(t) = \{A_1(t),..., A_L(t)\}$.
Due to the existence of the position bias, the agents may not effectively observe all the items.
Denote $O(t) \subseteq A(t)$ as the agnet's observed items in round $t$. 
Since the agent can also observe the information pertaining to the earlier agents' experience and he is bayesian rational and mypotic, then he will select the "perceived best" item based on his updated bayesian posterior.
Let $I_t$ be the selected item by the agent in time round $t$, then:
\begin{align}
I_t = \argmax_{j \in O(t)} \frac{s_{t-1}^j + s_0^j}{s_{t-1}^j + s_0^j + f_{t-1}^j + f_0^j}
\end{align}
Also let this selected item's position is $S_{t} \in \{1,...,L\}$, i.e., $A(t)[S_{t}] = I_t$.

\textbf{Learning:} We consider the revenue maximization problem of the principal, i.e., he wants to maximize the overall cumulative positive experience of the agents. 
Thus, the principal needs to design selection rule (algorithm) to incentivize the agents to select the best item. 
We measure the algorithm's performance via the standard definitions of \textit{regret}. 
Without loss of generality, assume that $\theta_1 > \theta_2 > ... > \theta_K$ and $\kappa_1 > \kappa_2> ...> \kappa_L$.
We are mainly interested in expected regret, where the expectation is taken over the randomness in the realized rewards and the algorithm: 
\begin{align}
\mathbb{E}[R(T)] = \sum_{t=1}^T (\theta_1 \kappa_1 - \theta_{I_t}\kappa_{S_{t}})
\end{align}
%the principal may either signify learning the true quality parameter of items (goods) or learning to forecast future outcomes (agnet's choices among these multiple items/goods).


\subsection{Voting Bias Model in a Social Network}
Feedback (Opinions) of individuals are often influenced by the private observations and opinion of friends, neighbors. 

\subsection{Choice Architecture in Bandit learning Problems}
Constructing the appropriate choice architecture is a powerful tool for affecting behavior.





\ignore{
	
\newpage
%----------------------------------------------------------------
\section{11 June, 2018}

\subsection{Selection Bias Model}
Let there're $K$ items with the unknown quality: $\{\theta_k\}_{1\leq k \leq K}$ and $\theta_k \in (0,1)$. 
The agent has a prior belief on which item has the best quality: $p_k = \mathbb{P}(\argmax_{j: 1\leq j \leq K}\theta_j = k)$.
We assume the agents arrived in the whole period are all homogenous.
Let $\{p_k^t\}_{1\leq k \leq K}$ be the agent $t$'s posterior belief on the probablity measure of which item is the best.
Clearly, by default, we have: $p_k^1 = p_k, \forall k \in [K]$.
\begin{itemize}
  \item at each round $t$, the agent will select the item according to his posterior belief $\{p_k^t\}_{1\leq k \leq K}$;
  \item once select an item to experience, the agent is required to report his truthful binary experience $X_t \in \{0,1\}$;
\end{itemize}

\textbf{Selection Rule - Bayesian Agents:} Bayesain agents will do bayesian learning when he reasons his posterior belief $\{p_k^t\}_{1\leq k \leq K}$:
\begin{equation} \label{bayesian-updating}
p_k^t = BU(p_k; X_1, ..., X_{t-1})
\end{equation}
where $BU(p_k; X_1, ..., X_{t-1})$ denotes the Bayesian update of $p_k$ given the observation feedback path $\{X_1, ..., X_{t-1}\}$.
\begin{itemize}
  \item Agent 1 will follow his own prior belief to select the best item and then report his experience;
  \item Agent 2 will observe that agent 1's decision and his experience, then select the item according to his updated posterior belief;
\end{itemize}

\textbf{Selection Rule - Non-Bayesian Agents:} Non-Bayesain agents will do non-bayesian learning when he reasons his posterior belief $\{p_k^t\}_{1\leq k \leq K}$:
\begin{equation} \label{non-bayesain-updating}
p_k^t = a_{tt} BU(p_k; X_1, ..., X_{t-1}) + \sum_{i=1}^{t-1}{a_{ti}p_k^i}
\end{equation}
where $a_{ti}>0$ captures the weight that agent $t$ assigns to the opinion of earlier agent $i$.
$a_{tt}$ is the weight that the agent assigns to her Bayesian posterior belief on her prior, which we refer to as the measure of \textit{self-reliance} of agent $t$. 
Note that the weights must satify $\sum_{i=1}^{t} a_{ti} = 1$. 
Observe that if $a_{ti} = 0$ for all $i\in [1,...,t-1]$ and for all $t$, then the updating rule \ref{non-bayesain-updating} will become \ref{bayesian-updating}.

\textbf{A Two-arm Example:} Consider there're two arms associated with unknown distinct quality parameter $\theta_1, \theta_2 \in (0,1)$. 
Agents have a common prior on which arm is the best:
\begin{align}
p_1 & = \mathbb{P}(\theta_1 > \theta_2) \\
p_2 & = \mathbb{P}(\theta_2 > \theta_1) = 1-p_1
\end{align}








\newpage
%----------------------------------------------------------------
\section{04 June 2018}
Consider following naive herding model in the selection phase: there're two items $1$ and $2$ (or two states) associated with unknown quality $\theta_1$ and $\theta_2$. 
\begin{itemize}
  \item at each round $t$, the agent needs to decide which item to choose to maximum her expected reward, agent has the access to the earlier decisions and the earlier agents' rewards. 
  Let $a_t \in \{1,2\}$ denote the decision of the agent $t$;
  \item the agent experience the item and then provide his binary reward $r_t \in \{0,1\}$: $\mathbb{E}[r_t] = \theta_{a_t}$
\end{itemize}
Let $G$ denote the event that $\theta_1 > \theta_2$, 
and $B$ denote the event that $\theta_1 < \theta_2$. 
Assume the agent's prior for which item has a higher quality is:
\begin{align}
   \mathbb{P}(\theta_1 > \theta_2) & = p \\
   \mathbb{P}(\theta_1 < \theta_2) & = 1-p
\end{align}
Without loss of generality, assume $p > 1/2$. \\
Then the 1st agent will choose the item $1$, i.e., $a_1 = 1$ and then get a reward $r_1$.\\
And the 2nd agent will then update his posterior:
\begin{align}
\mathbb{P}(G|a_1, r_1) & = \frac{\mathbb{P}(G)\mathbb{P}(a_1, r_1|G)}{\mathbb{P}(a_1, r_1)}\\
& = \frac{\mathbb{P}(G)\mathbb{P}(a_1, r_1|G)}{\mathbb{P}(G)\mathbb{P}(a_1, r_1|G) + \mathbb{P}(B)\mathbb{P}(a_1, r_1|B)} \\
& = \frac{\mathbb{P}(G)\mathbb{P}(r_1|G)}{\mathbb{P}(G)\mathbb{P}( r_1|G) + 0} \\
\end{align}
However, if the agent can not observe earlier agents' decisions, then for the 2nd agent:

\begin{table}[H]
\centering
\caption{The probability of receiving binary rewards, as a function of the two possible states}
\label{my-label}
\begin{tabular}{l|l|l}
reward\textbackslash{}state & $G(\theta_1 > \theta_2)$     &$B(\theta_1 < \theta_2)$      \\ \hline
$r_t = 1$                   & $\theta_1$      & $\theta_2$  \\ \hline
$r_t = 0$                   & $1-\theta_1$    & $1-\theta_2$
\end{tabular}
\end{table}

\begin{align}
\mathbb{P}(G|r_1=1) & = \frac{\mathbb{P}(G)\mathbb{P}(r_1=1|G)}{\mathbb{P}(r_1=1)}\\
& = \frac{\mathbb{P}(G)\mathbb{P}(r_1=1|G)}{\mathbb{P}(G)\mathbb{P}( r_1=1|G) + \mathbb{P}(B)\mathbb{P}(r_1=1|B)} \\
& = \frac{p \theta_1}{p\theta_1 + (1-p)\theta_2}
\end{align}
Suppose up to round $t$, the agent $t$ has observed a total of $t-1$ rewards with $S$ 1s and $t-1-S$ 0s. 


}




% %----------------------------------------------------------------
% \section{Conclusion}



\bibliographystyle{plainnat}
\bibliography{mybib}



\end{document}
